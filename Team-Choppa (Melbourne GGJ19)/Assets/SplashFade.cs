﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashFade : MonoBehaviour {

    private bool transparent = false;
    private bool isWaitFinished = false;
    public Image img;

    // Update is called once per frame
    void Start ()
    {
        
    }

    void Update () {
        if (!isWaitFinished)
        {
            StartCoroutine(Timer());
        } else if(!transparent)
        {
            StartCoroutine(FadeOut());
        }
	}

    IEnumerator FadeOut()
    {
        transparent = true;

        for (float alpha = 1.0f; alpha > 0f; alpha -= Time.deltaTime)
        {
            img.color = new Color(img.color.r, img.color.b, img.color.g, alpha);

            yield return null;
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(2f);
        isWaitFinished = true;
    }
}
