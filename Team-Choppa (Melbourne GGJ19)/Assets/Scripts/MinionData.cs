﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionData : MonoBehaviour {
	public AttackCardData cardData;
	public int health;

	void Start () {
		health = cardData.health;
	}
}
