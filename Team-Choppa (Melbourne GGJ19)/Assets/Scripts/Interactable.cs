﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    public SpawnCards spawnCards;

	public void OnMouseDown()
    {
        Debug.Log("Spawn object");
        spawnCards.Spawn();
    }
}
