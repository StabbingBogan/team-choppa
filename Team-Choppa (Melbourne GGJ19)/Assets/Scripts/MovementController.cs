﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

	public float movementSpeed;
	private float movementSpeedModifier = 10f;
	public Vector3 movementDirection;
	private Rigidbody2D rigidBody2D;

	void Start () {
		rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
	}

	void FixedUpdate () {
		rigidBody2D.MovePosition(gameObject.transform.position + (movementDirection * Time.fixedDeltaTime * movementSpeed / movementSpeedModifier));
	}
}
