﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Card: MonoBehaviour
{
    public AttackCardData minion;

    public Image artwork;
    public TMPro.TextMeshProUGUI health;
    public TMPro.TextMeshProUGUI attack;
    public TMPro.TextMeshProUGUI mouvementSpeed;

    public string minionType { get; set; }

    public GameObject playerCard { get; set; }

    void Start()
    {
        health.text = minion.health.ToString();
        attack.text = minion.attack.ToString();
        mouvementSpeed.text = minion.movementSpeed.ToString();
        artwork.sprite = minion.portrait;
    }
}