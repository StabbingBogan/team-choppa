﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCards : MonoBehaviour {

    public GameObject cardPrefab;
    public int maxCardNumber;

    public AttackCardData[] minions;


    public void Spawn()
    {
        if (transform.childCount >= maxCardNumber)
        {
            return;
        }
        GameObject cardClone = Instantiate(cardPrefab, transform.position, transform.rotation);

        cardClone.transform.SetParent(transform, false);

        Card card = cardClone.GetComponent<Card>();

        int itemIndex = Random.Range(0, minions.Length);
        Debug.Log("index card generated :" + itemIndex + " " + minions.Length);
        card.minion = minions[itemIndex];
    }

    private int GetNbCard()
    {
        return GetComponents<GameObject>().Length;
    }

}
