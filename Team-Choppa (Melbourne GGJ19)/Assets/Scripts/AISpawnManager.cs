using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawnManager : MonoBehaviour {

	public AttackCardData [] minions;
	public Vector2 movementDirection = Vector2.right;
	public float spawnInterval = 5f;
	private float spawnTick = 0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		spawnTick += Time.deltaTime;

	    if (spawnTick >= spawnInterval)
        {
			spawnTick = 0;
			SpawnMinion(GetMinion());
		}
	}

    AttackCardData GetMinion()
    {
        int itemIndex = Random.Range(0, (minions.Length - 1));

        return minions[itemIndex];
    }

    public void SpawnMinion(AttackCardData attackCardData)
	{
		Transform minionTransform = gameObject.transform;

		GameObject minionInstance = Instantiate(attackCardData.minionPrefab, minionTransform);
		minionInstance.GetComponent<MovementController>().movementSpeed = attackCardData.movementSpeed;
		minionInstance.GetComponent<MovementController>().movementDirection = movementDirection;
        minionInstance.GetComponent<MinionData>().cardData = attackCardData;
        minionInstance.tag = gameObject.tag;
		minionInstance.layer = gameObject.layer;
	}
}
