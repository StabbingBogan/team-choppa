﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAttackCard", menuName = "ScriptableObjects/AttackCard")]
public class AttackCardData : ScriptableObject {
    public string cardName;
    public string minionType;
    public Sprite portrait;
    
	public GameObject minionPrefab;

    public float coolDown;
    public float movementSpeed;
    public int health;
    public int attack;
    public string[] ignoreList;
}
