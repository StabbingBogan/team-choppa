using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

	public Vector2 movementDirection = Vector2.right;

	// Use this for initialization
	void Start () {
	}

	public void SpawnMinion(AttackCardData attackCardData)
	{
		Transform minionTransform = gameObject.transform;

		GameObject minionInstance = Instantiate(attackCardData.minionPrefab, minionTransform);
		minionInstance.GetComponent<MovementController>().movementSpeed = attackCardData.movementSpeed;
		minionInstance.GetComponent<MovementController>().movementDirection = movementDirection;
		minionInstance.GetComponent<MinionData>().cardData = attackCardData;
		minionInstance.tag = gameObject.tag;
		minionInstance.layer = gameObject.layer;
	}
}
