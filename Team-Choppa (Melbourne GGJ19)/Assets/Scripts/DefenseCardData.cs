﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDefenseCard", menuName = "ScriptableObjects/DefenseCard")]
public class DefenseCardData : ScriptableObject {
    public string cardName;
    public Sprite portrait;

    public float lifeCycle;
    public string[] ignoreList;
}
