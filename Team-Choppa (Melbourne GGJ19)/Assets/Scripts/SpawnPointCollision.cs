﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointCollision : MonoBehaviour {
	public void OnTriggerEnter2D(Collider2D collider) {
		if (gameObject.tag != collider.gameObject.tag) {
			Destroy(collider.gameObject);
		}
	}
}
