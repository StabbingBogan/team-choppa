﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public void CardDidDropInZone(Card card, SpawnManager spawnManager )
    {
        spawnManager.SpawnMinion(card.minion);
    }
}
