﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CombatManager : MonoBehaviour
{
    public bool isResolving = false;

    private GameObject _selfMinion;
    private GameObject _enemyMinion;

    private AttackCardData selfData;
    private AttackCardData enemyData;

    public void Setup(GameObject selfMinion, GameObject enemyMinion) {
        _selfMinion = selfMinion;
        _enemyMinion = enemyMinion;

        selfData = _selfMinion.GetComponent<MinionData>().cardData;
        enemyData = _enemyMinion.GetComponent<MinionData>().cardData;
    }

    public void Resolve ()
    {
        float attacksToKillEnnemy = _enemyMinion.GetComponent<MinionData>().health / selfData.attack;
        float attacksToBeKilled = _selfMinion.GetComponent<MinionData>().health / enemyData.attack;

        if (attacksToKillEnnemy > attacksToBeKilled) { // Enemy kills me
            int damage = (int) (Math.Floor(attacksToBeKilled) * selfData.attack); // Apply damage to enemy
            _enemyMinion.GetComponent<MinionData>().health -= damage;
            Destroy(_selfMinion);
        }
        else if (attacksToKillEnnemy < attacksToBeKilled) { // I kill enemy
            int damage = (int) (Math.Floor(attacksToKillEnnemy) * enemyData.attack); // Apply damage to myself
            _selfMinion.GetComponent<MinionData>().health -= damage;
            Destroy(_enemyMinion);
        }
        else { // We both die... \o/
            Destroy(_selfMinion);
            Destroy(_enemyMinion);
        }
    }

    // public void resolveTrap () //Should only be called once to determine if trap affects minion or not
    // {
    //     if (minion2.affectsMinion && minion2.affectsMinion == minion1.minionType)
    //     {
    //         Destroy(minion1);
    //     }

    //     if (minion1.affectsMinion && minion1.affectsMinion == minion2.minionType)
    //     {
    //         Destroy(minion2);
    //     }
    // }
}
