﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionCollision : MonoBehaviour {
	public void OnTriggerEnter2D(Collider2D collider) {
		// If minion collides with adversary game object
		if (gameObject.tag == "Player" && collider.gameObject.tag == "Enemy") {
			// And the gameobject we have collided with is not a spawn point
			if (collider.gameObject.GetComponent<SpawnPointCollision>() == null) {
				// Engage combat!
				CombatManager combatManager = gameObject.GetComponent<CombatManager>();
				CombatManager enemyCombatManager = collider.gameObject.GetComponent<CombatManager>();
				
				if (!enemyCombatManager.isResolving)
				{
					combatManager.isResolving = true;
					combatManager.Setup(gameObject, collider.gameObject);
					combatManager.Resolve();
					combatManager.isResolving = false;
				}
			}
		}
	}
}
