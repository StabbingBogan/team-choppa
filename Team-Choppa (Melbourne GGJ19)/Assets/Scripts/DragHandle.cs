﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandle : MonoBehaviour, IDragHandler , IEndDragHandler, IBeginDragHandler
{

    Vector3 parentToReturnTo = Vector3.zero;
    public void OnBeginDrag(PointerEventData eventData)
    {

        parentToReturnTo = this.transform.localPosition;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = parentToReturnTo;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }


}
