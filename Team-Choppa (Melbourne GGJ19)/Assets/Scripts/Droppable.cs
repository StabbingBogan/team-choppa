﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Droppable : MonoBehaviour, IDropHandler {

    public GameManager gameManager;

    public SpawnManager spawnManager;

    public void OnDrop(PointerEventData data)
    {
        Card card = data.pointerDrag.gameObject.GetComponent<Card>();
        gameManager.CardDidDropInZone(card, spawnManager);

        Destroy(data.pointerDrag.gameObject);
    }
}
